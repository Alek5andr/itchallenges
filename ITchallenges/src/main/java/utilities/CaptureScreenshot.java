package utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CaptureScreenshot {
	private WebDriver driver = BrowserFactory.getFactory().getDriver();
	private String screenshotName = ".\\testScreenShot.jpg";
	private final String OPERATING_SYSTEM = System.getProperty("os.name");
	
	public CaptureScreenshot() {
		if ("Linux".equals(OPERATING_SYSTEM)) {
			screenshotName = screenshotName.replaceAll("\\\\+", "/");
		}
		
		try {
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			FileUtils.copyFile(scrFile, new File(screenshotName));
		} catch (IOException e) {
			System.out.println("Could not capture screenshot.");
			e.printStackTrace();
		}
	}
}
