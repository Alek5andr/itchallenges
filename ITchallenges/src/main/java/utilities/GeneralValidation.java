/**
 * This class provides with methods for different kinds of validation for an element(s).
 */

package utilities;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.BrowserFactory;

public class GeneralValidation {

	private WebDriver driver = BrowserFactory.getFactory().getDriver();

	public String get_Title() {
		String title = driver.getTitle();
		return title;
	}

}
