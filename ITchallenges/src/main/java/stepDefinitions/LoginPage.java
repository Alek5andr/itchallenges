package stepDefinitions;

import pageObjects.LoginPagePO;

public class LoginPage {
	
	private LoginPagePO loginPageObjects = new LoginPagePO();
	
	public void enterEMailAddress(String email) {
		System.out.println("Entering '" + email + "' into 'E-Mail Address' field.");
		loginPageObjects.sendKeysToEmailAdressField(email);
	}
	
	public void enterPassword(String password) {
		System.out.println("Entering '" + password + "' into 'Password' field.");
		loginPageObjects.sendKeysToPasswordField(password);
	}
	
	public void logIn() {
		System.out.println("Clicking on 'Login' button.");
		loginPageObjects.clickLoginBtn();
	}

}
