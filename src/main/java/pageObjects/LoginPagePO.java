package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPagePO extends PageSetup {

	@FindBy(how = How.ID, using = "input-email")
	private WebElement emailAdressField;

	@FindBy(how = How.ID, using = "input-password")
	private WebElement passwordField;
	
	@FindBy(how = How.CSS, using = "input[value='Login']")
	private WebElement loginBtn;
	
	
	public void sendKeysToEmailAdressField(String email) {
		emailAdressField.sendKeys(email);
	}
	
	public void sendKeysToPasswordField(String password) {
		passwordField.sendKeys(password);
	}
	
	public void clickLoginBtn() {
		loginBtn.click();
	}
	
}
