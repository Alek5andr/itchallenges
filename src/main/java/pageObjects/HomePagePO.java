package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePagePO extends PageSetup {
	
	@FindBy(how = How.CSS, using = ".list-unstyled a[href$='account/account']")
	private WebElement myAccountLink;
	
	public void clickMyAccountLink() {
		myAccountLink.click();
	}
}
