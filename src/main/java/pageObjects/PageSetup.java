package pageObjects;

import org.openqa.selenium.support.PageFactory;

import utilities.BrowserFactory;

public class PageSetup {
	
	
	public PageSetup() {
		PageFactory.initElements(BrowserFactory.getFactory().getDriver(), this);
	}
}
