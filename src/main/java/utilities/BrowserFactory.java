package utilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.SessionId;

public class BrowserFactory {

	private static BrowserFactory factory;
	private WebDriver driver = null;
	private static SessionId session = null;
	private int pageTimeoutInSeconds;

	private final String OPERATING_SYSTEM = System.getProperty("os.name");
	private final String CHROMEDRIVER_LOCATION_WINDOWS = ".\\Drivers\\chromedriver_win32\\chromedriver.exe";
	private final String CHROMEDRIVER_LOCATION_UNIX = ""; // If you run tests on Unix machine, specify a path to the driver here.
	private String chromeDriverLocation = "";
	private static final String browserName = "Chrome";

	public WebDriver getDriver() {
		driver.manage().deleteAllCookies();
		return driver;
	}
	
	public SessionId getDriverSessionID() {
		switch (browserName) {
			default:
				throw new IllegalArgumentException("No such driver implemneted yet - " + browserName);
			case "Chrome":
				session = ((ChromeDriver)driver).getSessionId();
		}
		return session;
	}

	public int getPageTimeoutInSeconds() {
		return pageTimeoutInSeconds;
	}

	public static BrowserFactory getFactory() {
		if (factory == null) {
			factory = new BrowserFactory(browserName, 10);
		}
		return factory;
	}

	private BrowserFactory(String browserName, int implicitWaitTimeSeconds) {
		setTimeoutForOperatingSystem();
		System.out.println("\nOperating system is " + OPERATING_SYSTEM);
		System.out.println("Opening browser.");

		switch (browserName) {
			case "Chrome":
				ChromeDriverService service = new ChromeDriverService.Builder()
						.usingDriverExecutable(new File(chromeDriverLocation)).usingPort(4444).build();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--dns-prefetch-disable", "--start-maximized");
				driver = new ChromeDriver(service, options);
				break;

			default:
				throw new IllegalArgumentException("Unsupported browser type - " + browserName);
		}
		this.setupImplicitWaitSeconds(implicitWaitTimeSeconds);
	}

	private void setTimeoutForOperatingSystem() {
		if ("Windows 10".equals(OPERATING_SYSTEM)) {
			chromeDriverLocation = CHROMEDRIVER_LOCATION_WINDOWS;
			pageTimeoutInSeconds = 10;
		} else if("Windows 7".equals(OPERATING_SYSTEM)) {
			illegalArgumentExceptionThrower(OPERATING_SYSTEM);
		} else if ("Linux".equals(OPERATING_SYSTEM)) {
			chromeDriverLocation = CHROMEDRIVER_LOCATION_UNIX;
			pageTimeoutInSeconds = 20;
		} else {
			illegalArgumentExceptionThrower(OPERATING_SYSTEM);
		}
	}
	
	private void illegalArgumentExceptionThrower(String operatingSystem) {
		throw new IllegalArgumentException("Unsupported OS - " + operatingSystem);
	}

	private void setupImplicitWaitSeconds(int implicitWaitSeconds) {
		driver.manage().timeouts().implicitlyWait(implicitWaitSeconds, TimeUnit.SECONDS);
	}

}
