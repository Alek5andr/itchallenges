package utilities;

import org.openqa.selenium.WebDriver;

import utilities.BrowserFactory;

public class GeneralValidation {

	private WebDriver driver = BrowserFactory.getFactory().getDriver();

	public String get_Title() {
		String title = driver.getTitle();
		return title;
	}

}
