package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import pageObjects.HomePagePO;
import utilities.BrowserFactory;
import utilities.GeneralValidation;

import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class HomePage extends GeneralValidation {
	
	private WebDriver driver = BrowserFactory.getFactory().getDriver();
	private HomePagePO homePageObjects = new HomePagePO();
	
	public void navigateToHomePage(String url) {
		System.out.println("Navigating to " + url);
		try
        {
			driver.manage().timeouts().pageLoadTimeout(BrowserFactory.getFactory().getPageTimeoutInSeconds(), TimeUnit.SECONDS);
            driver.get(url);
        }
        catch (TimeoutException e)
        {
            performEscapeKeyPress();
        }
	}
	
	private void performEscapeKeyPress()
    {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE);
        action.build();
        action.perform();
    }
	
	public void assertTitle(String title) {
		System.out.println("Asserting title '" + title + "'.");
		Assert.assertEquals(super.get_Title(), title, "It is not '" + title + "'.");
	}
	
	public void clickMyAccountAtTheBottom() {
		System.out.println("Clicking on 'My Account' at the bottom.");
		homePageObjects.clickMyAccountLink();
	}

}
