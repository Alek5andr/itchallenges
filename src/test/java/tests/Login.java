package tests;

import org.testng.annotations.Test;

import stepDefinitions.HomePage;
import stepDefinitions.LoginPage;
import utilities.BrowserFactory;
import utilities.CaptureScreenshot;

import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

public class Login {
	
	private static boolean isInitialized;
	private HomePage homePage = new HomePage();	
	private LoginPage loginPage = new LoginPage();
	private final String URL = "https://127.0.0.1/opencart/"; // Put website's URL here.
	private final String YOUR_EMAIL = "YOUR@EMAIL.eu"; // Put your e-mail address here.
	private final String YOUR_PASSWORD = "123456"; // Put your password here.
	public static WebDriver driver;
	
  @Test
  public void loginTest () {
	  homePage.navigateToHomePage(URL);
	  homePage.assertTitle("Your Store");
	  homePage.clickMyAccountAtTheBottom();
	  homePage.assertTitle("Account Login");
	  loginPage.enterEMailAddress(YOUR_EMAIL);
	  loginPage.enterPassword(YOUR_PASSWORD);
	  loginPage.logIn();
	  homePage.assertTitle("My Account");
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  if (!isInitialized) {
			driver = BrowserFactory.getFactory().getDriver();
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					try {
						if (BrowserFactory.getFactory().getDriverSessionID() != null) {
							System.out.println("Closing browser.");
							driver.quit();
						}
					} catch (UnreachableBrowserException e) {
						System.out.println("Browser is closed/killed.");
					}
				}
			});
			
			isInitialized = true;
		}
		System.out.println("========== Start of Test ==========");
  }

  @AfterMethod
  public void afterMethod(ITestResult testResult) {
	  if (testResult.getStatus() == ITestResult.FAILURE){
			new CaptureScreenshot();
		}
		System.out.println("========== End of Test ==========\n");
  }

}
